// Author: Yakup Güzel
// Date: 30.10.2016
// control two leds with an I2C/TWI communication

#include <Wire.h> // include the Wire library

void setup() {
  // put your setup code here, to run once:
  Wire.begin(); // join the I2C bus as a master
  Wire.beginTransmission(0x20); // the slave address of your mcp23017; A0,A1,A2 are connected to GRND
  Wire.write(0x00); // IODIRA => 0x00; IODIRB => 0x01; look at datasheet page 9 of mcp23017
  Wire.write(0x00); // set the 8 pins as an output (0x00 -> 00000000); 0 = Output; 1 = Input;
  Wire.endTransmission(); // the puffered bytes from the method write() are transmitted now

void loop() {
  // put your main code here, to run repeatedly:
  Wire.beginTransmission(0x20); // begin transmission to slave with address 0x20
  Wire.write(0x12); // select bank A (GPIO A); look at datasheet page 9 of mcp23017
  Wire.write(0x1); // set the GPA0 pin to HIGH and the other pins to LOW
  Wire.endTransmission(); // transmit puffered bytes
  delay(500); // delay 500ms
  Wire.beginTransmission(0x20); // // begin transmission to slave with address 0x20
  Wire.write(0x12); // select bank A (GPIO A); look at datasheet page 9 of mcp23017
  Wire.write(0x80); // set the GPA7 pin to HIGH and the other pins to LOW
  Wire.endTransmission(); // transmit puffered bytes
  delay(500); // delay 500ms
}

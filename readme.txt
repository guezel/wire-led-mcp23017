--------------------
General Informations
--------------------
Website: https://yakup-guezel.de/
GitLabe: https://gitlab.com/guezel/

Author: Yakup G�zel
Date: 30.10.2016

Coding language: C
IDE: Arduino (Desktop-App)

------------------
About this project
------------------
The Wire library allows you to communicate with I2C / TWI devices.
In this example I show you how to develope two blink leds with an Arduino as a master and a
mcp23017 as a slave.

Datasheet for the 16-Bit I/O Expander:
http://ww1.microchip.com/downloads/en/DeviceDoc/21952b.pdf

-------------
Used librarys
-------------
- Wire library (#include <Wire.h>)
more informations about this lib you find at: https://www.arduino.cc/en/Reference/Wire